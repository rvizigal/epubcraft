import tkinter as tk
from tkinter import messagebox
import principal as pr
import config as con
from PIL import Image, ImageTk
from idlelib.tooltip import Hovertip

def alerta(mensagem):
    messagebox.showinfo("Alerta - EPUBCraft", mensagem)

def on_submit():
    repositorio = entry_repositorio.get()
    livro = entry_livro.get()
    con.atualizar_config(livro,repositorio)
    if livro != "" and repositorio != "":
        pr.criar_livro(livro, repositorio)
        alerta("Concluido com sucesso")
    else:
        alerta("Campo livro e repositório são obrigatórios")
def status(status):
    if status == 1:
        label_repositorio.config(text='Status: Aguardando')
    elif status == 2:
        label_repositorio.config(text='Status: Criando')

# Criar a janela principal
root = tk.Tk()
root.title("1.0.0 - EPUBCraft by Vizigal")
root.geometry("435x200")

#carrega configuração do json
dados = con.ler_config()

# Carregar a imagem
image = Image.open("./Imagem/logo.png")
# Redimensionar a imagem para o tamanho desejado
image = image.resize((100, 50))
# Converter a imagem para o formato compatível com o Tkinter
photo = ImageTk.PhotoImage(image)

label_url = tk.Label(root, image=photo)
label_url.grid(row=0, column=0,padx=5, pady=5, sticky=tk.NSEW)
label_url = tk.Label(root, text="Bem vindo ao EPUBCraft!")
label_url.grid(row=0, column=1, padx=5, pady=5, sticky=tk.W)

label_livro = tk.Label(root, text="Livro:")
label_livro.grid(row=4, column=0, padx=5, pady=5, sticky=tk.W)
entry_livro = tk.Entry(root, width=50)
entry_livro.grid(row=4, column=1, padx=5, pady=5)
entry_livro.insert(tk.END, dados["livro"])
myTip = Hovertip(entry_livro,
                 'Caso não seja informado ira criar com um nome default Livro')

label_repositorio = tk.Label(root, text="Repositorio:")
label_repositorio.grid(row=5, column=0, padx=5, pady=5, sticky=tk.W)
entry_repositorio = tk.Entry(root, width=50)
entry_repositorio.grid(row=5, column=1, padx=5, pady=5)
entry_repositorio.insert(tk.END, dados["repositorio"])
myTip = Hovertip(entry_repositorio,
                 'Caso não seja informado ira salvar no repositorio padrão da execução(Exemplo:D:/)')



# Criar botão de envio
submit_button = tk.Button(root, text="Criar EPUB", command=on_submit)
submit_button.grid(row=6, columnspan=2, padx=5, pady=10)
myTip = Hovertip(
    submit_button, 'Iniciar o processo de criação')

label_repositorio = tk.Label(root, text="Status:")
label_repositorio.grid(row=7, column=0, padx=5, pady=5, sticky=tk.W)
status(1)

# Iniciar o loop de eventos
root.mainloop()