import json

def cria_config():
    # Dados a serem armazenados no JSON
    dados = {
        "livro": "",
        "repositorio": ""
    }
    # Nome do arquivo JSON que iremos criar
    nome_arquivo = "config.json"
    # Escrever o arquivo JSON
    with open(nome_arquivo, "w") as arquivo:
        json.dump(dados, arquivo)  
    return ler_config()

def atualizar_config(livro = 'Livro',repositorio=''):
    dados = ler_config()
    dados["livro"] = livro
    dados["repositorio"] = repositorio
    with open("config.json", "w") as arquivo:
        json.dump(dados, arquivo, indent=4)


def ler_config():
    try:
        with open("config.json", "r") as arquivo:
            conteudo = arquivo.read()
            dados = json.loads(conteudo)
    except FileNotFoundError:    
        return cria_config()
    return dados