import os
from PIL import Image
from ebooklib import epub
import glob
import io

#Criar autor
def criar_autor(book, livro):
    # set metadata
    book.set_identifier("98 117 121")
    # titulo
    book.set_title(livro)
    book.set_language("pt-br")

    book.add_author("Autor EPUBCraft")
    book.add_author(
        "by Vizigal",
        file_as="não me responsabilizo por isso!",
        role="br",
        uid="creator",
    )
    return book


#criar imagens
def criar_imagens(book, imagem, repositorio,capa=False):
    if repositorio == '': repositorio='.'
    imagens = open(repositorio + '/' + imagem, "rb").read()

    if capa == True:
        image = Image.open(io.BytesIO(imagens))
        #redimenciona imagem da capa
        imagem_resized = image.resize((400, 500))
        buffer_imagem = io.BytesIO()
        imagem_resized.save(buffer_imagem, format="PNG")
        imagens = buffer_imagem.getvalue()
    img = epub.EpubImage(
        uid=imagem,
        file_name="static/" + imagem,
        media_type="image/png",
        content=imagens,
    )
    if capa == True:
        book.set_cover("static/capa.png", img.content)
    else:
        # add imagem
        book.add_item(img)
    return book


# add imagens com codigo
def add_imagens(lista_imagem, imagem):
    lista_imagem += '<p><img alt="[ebook logo]" src="static/' + \
        imagem + '"/><br/></p>'
    #print(lista_imagem)
    return lista_imagem

#criar capitulo
def criar_capitulo(book,lista_imagem, livro):
    #verificar inclusão de capa
    capa = "static/capa.png"
    c1 = epub.EpubHtml(title=livro, file_name=livro + ".xhtml", lang="hr")
    c1.content = (
        f"""
        <!DOCTYPE html>
        <html>
        <head>
            <title>{livro}</title>
            <style>
                body {{
                    margin: 0;
                    padding: 0;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 100vh;
                    background-color: #e8e8e8;
                }}
                .capa-container {{
                    text-align: center;
                    max-width: 95%;
                }}
                .capa-img {{
                    max-width: 100%;
                    max-height: 100%;
                }}
                .capa-text {{
                    font-size: 24px;
                    color: #333;
                    margin-top: 20px;
                }}
            </style>
        </head>
        <body>
            <div class="capa-container">
                <center>
                    <p class="capa-text"><h2>{livro}</h2></p>
                    <img class="capa-img" src="{capa}" alt="Capa do Livro">
                </center>
            </div>
            <center><p><img alt="[ebook logo]" src="static/contraCapa.jpg"/><br/></p></center>
            <center>{lista_imagem}</center>
        </body>
        </html>
        """
    )
    # add capitulo
    book.add_item(c1)
    # basic spine
    book.spine = [c1]
    return book

#finalizar o livro
def finalizar_livro(book,livro,repositorio):
    # add default NCX and Nav file
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    # define CSS style
    style = "BODY {color: white;}"
    nav_css = epub.EpubItem(
        uid="style_nav",
        file_name="style/nav.css",
        media_type="text/css",
        content=style,
    )
    # add CSS file  
    book.add_item(nav_css)
    # write to the file
    epub.write_epub(repositorio + '/'+ livro + '.epub', book, {})

#nome das imagens no repositorio


def obter_nomes_imagens(repositorio, extensao='.jpg'):
    nomes_imagens = []
    padrao_jpg = os.path.join(repositorio, '*'+extensao)
    for nome_arquivo in glob.glob(padrao_jpg):
        nomes_imagens.append(os.path.basename(nome_arquivo))
    return nomes_imagens

#criar livro
def criar_livro(livro,repositorio):
    book = epub.EpubBook()
    book = criar_autor(book, livro)
    #pega o nome de todas as imagens jpg do repositorio
    nomes_imagens = obter_nomes_imagens(repositorio)
    lista_imagem = ''
    # Adicionar a primeira imagem como a capa do livro
    criar_imagens(book, nomes_imagens[0], repositorio, True)
    # Adicionar contra capa
    criar_imagens(book, 'contraCapa.jpg', './imagem')
    for nome_imagem in nomes_imagens:
        book = criar_imagens(book, nome_imagem, repositorio)
        lista_imagem = add_imagens(lista_imagem, nome_imagem)
    book = criar_capitulo(book, lista_imagem, livro)
    finalizar_livro(book, livro, repositorio)