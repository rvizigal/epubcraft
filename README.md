# EPUBCraft

![Alt text](Imagem/logo.png)

# Aplicação de Geração de Livro Digital ePub a partir de Imagens

Essa aplicação permite que você crie facilmente um livro digital no formato ePub a partir de uma coleção de imagens '.jpg'.

## Funcionalidades

A funcionalidade de geração de livro digital ePub a partir de imagens .jpg permite que você crie um livro digital no formato ePub fornecendo o nome do livro e o repositório onde as imagens estão localizadas. Além disso, a funcionalidade cria um arquivo `config.json` para armazenar os dados utilizados no processo de criação do livro.

**Interface do usuario**

![Alt text](Imagem/readme1.png)
___
**Exemplo de JSON de config:**
```json
{
    "livro": "livro",
    "repositorio": "D:/Git/manga/One Piece/One Piece Capitulo 1089/"
}
```

## Instalação dos imports via pip
O pip é uma ferramenta essencial para qualquer desenvolvedor Python, pois simplifica o processo de gerenciamento de pacotes e bibliotecas, permitindo que você concentre-se em criar, desenvolver e aprimorar seus projetos sem se preocupar com as complexidades de dependências e instalações.

### 1. Instalação do Tkinter
___
O módulo `tkinter` é utilizado para criar interfaces gráficas em Python. Geralmente, ele já está incluído na instalação padrão do Python, mas em alguns casos pode ser necessário instalá-lo.

```bash
pip install tk
```

### 2. Instalação do Pillow (Python Imaging Library)
___
O módulo PIL (Python Imaging Library) é usado para trabalhar com imagens em Python. Para instalá-lo, execute o seguinte comando

```bash
pip install Pillow
```

### 3. Instalação do módulo idlelib
___
O módulo idlelib fornece uma série de utilitários para a IDE IDLE (Python's Integrated Development and Learning Environment), incluindo o Hovertip que você está usando.

Certifique-se de que o módulo idlelib esteja incluído na sua instalação padrão do Python. Normalmente, não é necessário instalá-lo separadamente.

### 4. Instalação do módulo json
___
O módulo json é utilizado para trabalhar com dados no formato JSON. Ele faz parte da instalação padrão do Python, portanto, não é necessário instalá-lo separadamente.

### 5. Instalação do módulo ebooklib
___
O módulo ebooklib é utilizado para criar eBooks no formato ePub. Para instalá-lo, execute o seguinte comando:

```bash
pip install ebooklib
```

### 6. Instalação do módulo glob
___
O módulo glob é usado para encontrar todos os caminhos que correspondem a um padrão especificado de acordo com as regras utilizadas pelo shell Unix. Ele faz parte da instalação padrão do Python, portanto, não é necessário instalá-lo separadamente.

### 7. Instalação do módulo io
___
O módulo io é utilizado para trabalhar com fluxos de entrada/saída. Ele faz parte da instalação padrão do Python, portanto, não é necessário instalá-lo separadamente.

## Conclusão

Após seguir esses passos e instalar os módulos necessários, você estará pronto para utilizar a sua aplicação Python, que faz uso dos diversos módulos, como tkinter, PIL, idlelib, json, ebooklib, glob e io.

## Exemplo de execução
A baixo segue um exemplo de execução da aplicação.

![Alt text](Imagem/readme2.png)
___
# Licença de Uso

**IMPORTANTE: LEIA ATENTAMENTE ANTES DE UTILIZAR ESTE SOFTWARE.**

## Termos e Condições

Este é um acordo legal entre você (doravante referido como "Usuário") e Ramon Vizigal (doravante referido como "Autor"), relacionado ao software fornecido juntamente com esta licença. Ao utilizar este software, você concorda com os seguintes termos e condições:

1. **Uso Pessoal:** Este software pode ser usado tanto para fins pessoais quanto acadêmicos.

2. **Modificações:** O Usuário tem permissão para modificar, adaptar e melhorar o software de acordo com suas necessidades.

3. **Distribuição:** O Usuário pode distribuir este software, desde que mantenha os créditos e esta licença intactos em todas as cópias distribuídas.

4. **Sem Garantias:** O software é fornecido "como está", sem garantias de qualquer tipo, expressas ou implícitas. O Autor não se responsabiliza por quaisquer danos decorrentes do uso deste software.

5. **Suporte:** Este software é oferecido sem suporte técnico ou garantia de atualizações contínuas por parte do Autor.

## Direitos Autorais

Este software e sua documentação são protegidos por direitos autorais. Todos os direitos não concedidos explicitamente nesta licença são reservados ao Autor.

## Entre em Contato

Se tiver dúvidas sobre este código ou licença, entre em contato com Ramon Vizigal através do [linkedin](https://www.linkedin.com/in/ramon-vizigal-90164131/)

---

**Agradeço por usar este software.**

Licença criada por Ramon Vizigal.
